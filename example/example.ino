#include <ESP8266WiFi.h>
#include <SocketIOClient.h>

SocketIOClient socket;

const char* ssid     = "SSID";
const char* password = "PASSWORD";

//Change host/server IP
char host[] = "192.168.10.133";
int port = 3000;
extern String RID;
extern String Rname;
extern String Rcontent;
unsigned long previousMillis = 0;
unsigned long previousMillis2 = 0;
long interval = 5000;
long interval2 = 60000;

//CHIP ID => ESP8266
String chipID = "ESP_" + String(ESP.getChipId());

void setup() {

  Serial.begin(115200);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi conetado!");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  while (!socket.connect(host, port)) {
    Serial.println(F("Connection failed... Wait to reconnect!"));
    socket.reconnect(host, port);
    delay(2000);
  }
  if (socket.connected())
  {
    socket.send("connection", chipID, "connected!");
  }

}

void ledControl(String state) {
  if (state == "\"on\"") {
    Serial.println("[led] ON");
    digitalWrite(LED_BUILTIN, LOW);
  }
  else {
    Serial.println("[led] OFF");
    digitalWrite(LED_BUILTIN, HIGH);
  }
}

void ping() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    socket.heartbeat(0);
  }
}

void dataSend() {
  unsigned long currentMillis2 = millis();
  if (currentMillis2 - previousMillis2 > interval2) {
    previousMillis2 = currentMillis2;
    socket.send("atime", "message", "Time please?");
  }
}

void loop() {
  dataSend();
  ping();
  if (socket.monitor()) {
    if (RID == "led") {
      ledControl(Rcontent);
    }
  }
}




