var util  = require('util');
var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');
app.listen(3000,'0.0.0.0',function() {
  console.log('Server connected in port 3000');
});

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

function ParseJson(jsondata) {
    try {
        return JSON.parse(jsondata);
    } catch (error) {
        return null;
    }
}
function sendTime() {
    io.sockets.emit('atime', { time: new Date().toJSON() });
}

io.on('connection', function (socket) {

  //Received from index.html
  socket.emit('welcome', { message: 'Connected!' });

  //Received from ESP8266
  socket.on('connection', function (data) {
    console.log('ESP8266 => '+JSON.stringify(data));   
  });

  //Received from ESP8266
  socket.on('atime', function (data) {
    sendTime();
    console.log(data);
  });

  //Index.html > app.js > ESP8266
  socket.on('ledOff', function (data) {
    io.sockets.emit('led', { state: 'off' });
    console.log('Console:'+data);
  });

  //Index.html > app.js > ESP8266
  socket.on('ledOn', function (data) {
    io.sockets.emit('led', { state: 'on' });
    console.log('Console:'+data);
  });

  socket.on('disconnect', function(){
    console.log('User disconnected: ' + socket.id);
  });
});