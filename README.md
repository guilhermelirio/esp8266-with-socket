**ESP8266 with Socket.IO example**

Example of communication between an ESP8266, NodeJS server and an HTML page.

The following example will turn on / off the LED (LED_BUILTIN) of the nodeMCU via socket.

The lib I used as an example was: [Socket.io-v1.x-Library](https://github.com/washo4evr/Socket.io-v1.x-Library), but I made a change because the socket.monitor () function did not work;

---

## Installation

1. Clone git.
2. Install de library *Socket.io-v1.x-Library-master.rar* in de Arduino IDE.
3. Change the SSID, password and host inside example.ino.
4. Run app.js (need to have the node installed).
5. Upload the code to nodeMCU;
6. Open index.html code and edit IP to connect to your NodeJS server.
7. Open index.html with yout favorite browser.
8. Open Serial Monitor and app.js window to follow the console.log.


## Notes

Sorry for my bad english!

